object SettingsForm: TSettingsForm
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'SettingsForm'
  ClientHeight = 327
  ClientWidth = 609
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 8
    Top = 8
    Width = 593
    Height = 281
    ActivePage = TabSheet2
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'General'
      object cbAutoStart: TCheckBox
        Left = 16
        Top = 16
        Width = 201
        Height = 17
        Caption = 'Start with Windows'
        Enabled = False
        TabOrder = 0
      end
      object cbStayOnTop: TCheckBox
        Left = 16
        Top = 48
        Width = 201
        Height = 17
        Caption = 'Stay on Top'
        TabOrder = 1
        OnClick = cbStayOnTopClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Panel'
      ImageIndex = 1
      object Label1: TLabel
        Left = 16
        Top = 61
        Width = 83
        Height = 13
        Caption = 'Transparent: 0%'
      end
      object txtPanelSkinName: TEdit
        Left = 16
        Top = 16
        Width = 530
        Height = 21
        ReadOnly = True
        TabOrder = 0
        Text = 'panelStandard.png'
      end
      object Button1: TButton
        Left = 552
        Top = 14
        Width = 25
        Height = 25
        Caption = '...'
        TabOrder = 1
      end
      object tbPanelTransparent: TTrackBar
        Left = 16
        Top = 80
        Width = 561
        Height = 45
        Max = 255
        Frequency = 2
        TabOrder = 2
        OnChange = tbPanelTransparentChange
      end
    end
  end
  object btnOk: TButton
    Left = 526
    Top = 295
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 1
    OnClick = btnOkClick
  end
end
