unit NM2Utils;

interface

uses
  windows, sysutils, messages, classes, graphics, pngImage, NGraphicsUnit;

const
  IconRectIdent = 8;

type
  // ������ ��������� ������ (����� �� ��������� ���� ����������)
  TMenuButton = record
    iconEnabled: TBitmap;
    iconDisabled: TBitmap;

    iconEnTrans: TBitmap;
    iconDisTrans: TBitmap;

    enabled: boolean;
    Hint: string;
    pos: TPoint;
  end;



  // abstract item class
  TAbstractItem = class
  private
    FParent: TAbstractItem;
    FLocation: TPoint;
    FText: string;

    // 32 -bit bitmaps
    FIconFile: string;
    FIcon: TBitmap; // source
    FNormIcon: TBitmap; // icon with normal size for painting
    FBigIcon: TBitmap;
    // icon with extended size for painting selection of this item

  protected
    function getLeft: integer;
    function getTop: integer;
    procedure setLeft(const Value: integer);
    procedure setTop(const Value: integer);

    procedure setIcon(const icon: TPngImage); overload;
    procedure setIcon(const icon: TBitmap); overload;
    procedure setParent(const Value: TAbstractItem);

  public
    constructor Create(const AParent: TAbstractItem); virtual;
    destructor Destroy(); virtual;

    // element display-name
    property Text: string read FText write FText;
    // parent node on tree
    property Parent: TAbstractItem read FParent write setParent;

    // element location
    function getLocation(): TPoint;
    property Left: integer read getLeft write setLeft;
    property Top: integer read getTop write setTop;
    procedure setLocation(const x, y: integer); overload;
    procedure setLocation(const location: TPoint); overload;

    // icon (must be 32 bit)
    procedure setIcon(const fileName: string); overload;

    property iconNrmSize: TBitmap read FNormIcon;
    property iconExSize: TBitmap read FBigIcon;

    function getFullSizeIcon(): TBitmap;

    // create normal-size and extended size icons
    procedure iconCreateSizes();

  end;


  // -- tree node (subfolder or submenu)
  TItemList = array of TAbstractItem;
  TFolderItem = class(TAbstractItem)
  private
    FItems: TItemList;
    function getCount: integer;
    function getItem(index: integer): TAbstractItem;

  protected

  public
    constructor Create(const AParent: TAbstractItem); override;
    destructor Destroy(); override;

    property Item[index: integer]: TAbstractItem read getItem; default;
    property Count: integer read getCount;

    // ������ �������� �� ��� ����������� �������� (�����������)
    function findItemAt(const p: TPoint): integer;

  end;


  // -- link for some Windows-object (file, folder, etc)
  TLinkItem = class(TAbstractItem)
  private
     FFileName: string;
     FWorkFolder: string;
     FParameters: string;

  protected

  public
    constructor Create(const AParent: TAbstractItem); override;
    destructor Destroy(); override;

  end;


// settings
var
  iconNormSize: integer; // normal icon size (for example: 48)
  iconExtendSize: integer; // additional amount pixels for extended icon size (for ex.: 16);
  iconDefFolder: TBitmap;
  iconDefLink: TBitmap;

  iconDir: string;


  // ����������� 32-������ �������� + ������������ ����������� ��� ���������������
  // source - ��������
  // dest - �������� �����������. ������� ����� ������ �������
  procedure bm32Copy(const source, dest: TBitmap);
  //procedure bm32Copy2(const source, dest: TBitmap);

  // ������� ������ ������� RGBA, ��� A - ��� ����� ������������
  function AlphaBlendRGBA(color1, color2: TColor): TColor;

  function buildPng2bm32(png: string): TBitmap;

  // ������ ��������� �������
  procedure bm32Clear(const map: TBitmap);

  // ������� ��������������
  procedure bm32DownTransparent(const map: TBitmap; const val: byte);

  // ������ ������ ������
  procedure bm32Fill(const map: TBitmap; const color: TColor);

  // ����� �������
  function pointsLength(const p1, p2: TPoint): integer;


  // ������ ������ 48x48
  function createMenuButton(const pngEnabled, pngDisabled: string): TMenuButton;


implementation

uses
  MainUnit;

var
  form: TMainForm;

function pointsLength(const p1, p2: TPoint): integer;
begin
  result := round(sqrt( sqr(p1.X - p2.X) + sqr(p1.Y - p2.Y) ));
end;

function createMenuButton(const pngEnabled, pngDisabled: string): TMenuButton;
var
  bm1, bm2: TBitmap;
begin
  //bm1 := TBitmap.Create;
  //bm2 := TBitmap.Create;
  bm1 := buildPng2bm32(pngEnabled);
  bm2 := buildPng2bm32(pngDisabled);

  result.iconEnabled := TBitmap.Create;
  result.iconEnabled.PixelFormat := pf32bit;
  result.iconEnabled.SetSize(48, 48);
  bm32copy(bm1, result.iconEnabled);

  result.iconDisabled := TBitmap.Create;
  result.iconDisabled.PixelFormat := pf32bit;
  result.iconDisabled.SetSize(48, 48);
  bm32copy(bm2, result.iconDisabled);

  result.iconEnTrans := TBitmap.Create;
  result.iconEnTrans.Assign(result.iconEnabled);
  bm32DownTransparent(result.iconEnTrans, 128);

  result.iconDisTrans := TBitmap.Create;
  result.iconDisTrans.Assign(result.iconDisabled);
  bm32DownTransparent(result.iconDisTrans, 128);


  bm1.Free;
  bm2.Free;

  result.enabled := true;
  result.Hint := '';
end;

procedure bm32Fill(const map: TBitmap; const color: TColor);
var
  i, j: integer;
  p: PRGBQuad;
  clr: TRGBQuad;
begin
  clr := TRGBQuad(color);
  for j := 0 to map.Height - 1 do
  begin
    p := map.ScanLine[j];
    for i := 0 to map.Width - 1 do
    begin
      p^ := clr;
      inc(p);
    end;
  end;
end;

procedure bm32Copy(const source, dest: TBitmap);
var
  src, dst: TBitmap;
  prgb: PRGBTriple;
  prgba: PRGBQuad;
  i, j: integer;
begin
  src := TBitmap.Create;
  dst := TBitmap.Create;
  src.PixelFormat := pf24bit;
  dst.PixelFormat := pf24bit;

  // RGB
  src.SetSize(source.Width, source.Height);
  for j := 0 to source.Height - 1 do
  begin
    prgba := source.ScanLine[j];
    prgb := src.ScanLine[j];
    for i := 0 to source.Width - 1 do
    begin
      prgb^.rgbtRed := prgba^.rgbRed;
      prgb^.rgbtGreen := prgba^.rgbGreen;
      prgb^.rgbtBlue := prgba^.rgbBlue;
      inc(prgb);
      inc(prgba);
    end;
  end;

  dst.SetSize(dest.Width, dest.Height);

  i := SetStretchBltMode(dst.Canvas.Handle, STRETCH_HALFTONE);
  dst.Canvas.CopyRect(dst.Canvas.ClipRect, src.Canvas, src.Canvas.ClipRect);
  SetStretchBltMode(dst.Canvas.Handle, i);

  for j := 0 to dest.Height - 1 do
  begin
    prgb := dst.ScanLine[j];
    prgba := dest.ScanLine[j];
    for i := 0 to dest.Width - 1 do
    begin
      prgba^.rgbRed := prgb.rgbtRed;
      prgba^.rgbGreen := prgb.rgbtGreen;
      prgba^.rgbBlue := prgb.rgbtBlue;
      prgba^.rgbReserved := 0;
      inc(prgba);
      inc(prgb);
    end;
  end;

  // alpha
  for j := 0 to source.Height - 1 do
  begin
    prgba := source.ScanLine[j];
    prgb := src.ScanLine[j];
    for i := 0 to source.Width - 1 do
    begin
      prgb^.rgbtRed := prgba^.rgbReserved;
      prgb^.rgbtGreen := prgba^.rgbReserved;
      prgb^.rgbtBlue := prgba^.rgbReserved;
      inc(prgb);
      inc(prgba);
    end;
  end;

  i := SetStretchBltMode(dst.Canvas.Handle, STRETCH_HALFTONE);
  dst.Canvas.CopyRect(dest.Canvas.ClipRect, src.Canvas, source.Canvas.ClipRect);
  SetStretchBltMode(dst.Canvas.Handle, i);

  for j := 0 to dest.Height - 1 do
  begin
    prgb := dst.ScanLine[j];
    prgba := dest.ScanLine[j];
    for i := 0 to dest.Width - 1 do
    begin
      prgba^.rgbReserved := prgb^.rgbtGreen;
      inc(prgb);
      inc(prgba);
    end;
  end;

  src.Free;
  dst.Free;

end;

procedure bm32Copy2(const source, dest: TBitmap);
type
  PRGBQuadArray = ^TRGBQuadArray;
  TRGBQuadArray = array[0..10240] of TRGBQuad;
var
  i, j, k, flag_interpolation: integer;
  src: array of array of TRGBQuad; //indexation: [row, column]
  dst: PRGBQuadArray;

  m0, n0, m1, n1: integer; // ������� �����������

  x, y, dx, dy: single;
  p1, p2, p3, p4, p: TRGBQuad;
  x1, x2, y1, y2: integer;
begin
  flag_interpolation := 1;
  setLength(src, source.Width, source.Height);

  // cols [columns, OX]
  m0 := source.Width - 1;
  m1 := dest.Width - 1;
  // rows [lines, OY]
  n0 := source.Height - 1;
  n1 := dest.Height - 1;

  for j := 0 to n0 do
  begin
    dst := PRGBQuadArray(source.ScanLine[j]);
    for i := 0 to m0 do
      src[i, j] := dst[i];
  end;

  for j := 0 to n1 do // = OY
  begin
    dst := PRGBQuadArray(dest.ScanLine[j]);
    for i := 0 to m1 do // = OX
    begin
      x := (i / m1) * m0;
      y := (j / n1) * n0;

      // ������������
      x1 := trunc(x);
      y1 := trunc(y);

      dx := x - x1;
      dy := y - y1;

      dx := dx * dx;
      dy := dy * dy;

      x2 := x1 + 1;
      y2 := y1 + 1;

      If x2 > m0 then
        x2 := m0;
      If y2 > n0 then
        y2 := n0;
      p1 := src[x1, y1];
      p2 := src[x2, y1];
      p3 := src[x1, y2];
      p4 := src[x2, y2];

      p1.rgbBlue := p1.rgbBlue + Round(dx * (p2.rgbBlue - p1.rgbBlue));
      p1.rgbGreen := p1.rgbGreen + Round(dx * (p2.rgbGreen - p1.rgbGreen));
      p1.rgbRed := p1.rgbRed + Round(dx * (p2.rgbRed - p1.rgbRed));
      p1.rgbReserved := p1.rgbReserved + Round(dx * (p2.rgbReserved - p1.rgbReserved));

      p3.rgbBlue := p3.rgbBlue + Round(dx * (p4.rgbBlue - p3.rgbBlue));
      p3.rgbGreen := p3.rgbGreen + Round(dx * (p4.rgbGreen - p3.rgbGreen));
      p3.rgbRed := p3.rgbRed + Round(dx * (p4.rgbRed - p3.rgbRed));
      p3.rgbReserved := p3.rgbReserved + Round(dx * (p4.rgbReserved - p3.rgbReserved));

      p.rgbBlue := p1.rgbBlue + Round(dy * (p3.rgbBlue - p1.rgbBlue));
      p.rgbGreen := p1.rgbGreen + Round(dy * (p3.rgbGreen - p1.rgbGreen));
      p.rgbRed := p1.rgbRed + Round(dy * (p3.rgbRed - p1.rgbRed));
      p.rgbReserved := p1.rgbReserved + Round(dy * (p3.rgbReserved - p1.rgbReserved));

      dst[i] := p;
    end;
  end;
end;

function AlphaBlendRGBA(color1, color2: TColor): TColor;
var
  r1, g1, b1, a1, r2, g2, b2, a2, a, b: word;
  n: word;
begin
  a1 := byte(color1 shr 24);
  a2 := byte(color2 shr 24);
  if (a2 = 255) or (a1 = 0) then
  begin
    result := color2;
    exit;
  end;
  r2 := byte(color2);
  g2 := byte(color2 shr 8);
  b2 := byte(color2 shr 16);
  r1 := byte(color1);
  g1 := byte(color1 shr 8);
  b1 := byte(color1 shr 16);

  if (a1 = 0) and (a2 = 0) then
  begin
    result := 0;
    exit;
  end;

  b := a1 * (255 - a2) div 255;

  a := a2 + b;
  if a > 0 then
  begin
    r1 := (r2*a2 + (r1*b)) div a;
    g1 := (g2*a2 + (g1*b)) div a;
    b1 := (b2*a2 + (b1*b)) div a;
  end
  else
  begin
    result := 0;
    exit;
  end;
  result := r1 or (g1 shl 8) or (b1 shl 16) or (a shl 24);
end;

function buildPng2bm32(png: string): TBitmap;
const
  MaxPixelCountA = 10240;
type
  PRGBAArray = ^TRGBAArray;
  TRGBAArray = array [0..MaxPixelCountA - 1] of TRGBQuad;
var
  j, i: Integer;
  PNB: TPngObject;
  fff: PRGBAArray;
  aaa: pByteArray;
  rng: Integer;
  bmp: TBitmap;
begin
  result := nil;
  bmp := TBitmap.Create();
  PNB := TPngObject.Create;
  try
    PNB.LoadFromFile(png);
    PNB.CreateAlpha;
    bmp.Assign(PNB);
    bmp.PixelFormat := pf32bit;
    for i := 0 to bmp.Height - 1 do
    begin
      fff := bmp.ScanLine[i];
      aaa := PNB.AlphaScanline[i];
      for j := 0 to bmp.Width - 1 do
        fff[j].rgbReserved := aaa[j];
    end;
    result := bmp;
  finally
    PNB.free;
  end;
end;

procedure bm32DownTransparent(const map: TBitmap; const val: byte);
var
  p: PRGBQuad;
  i, j: integer;
begin
  for j := 0 to map.Height - 1 do
  begin
    p := map.ScanLine[j];
    for i := 0 to map.Width - 1 do
    begin
      p^.rgbReserved := p^.rgbReserved * val div 255;
      inc(p);
    end;
  end;
end;

procedure bm32Clear(const map: TBitmap);
var
  p: PRGBQuad;
  rgba: TRGBQuad;
  i, j: integer;
begin
  rgba.rgbBlue := 0;
  rgba.rgbGreen := 0;
  rgba.rgbRed := 0;
  rgba.rgbReserved := 0;
  for j := 0 to map.Height - 1 do
  begin
    p := map.ScanLine[j];
    for i := 0 to map.Width - 1 do
    begin
      p^ := rgba;
      inc(p);
    end;
  end;
end;


{ TAbstractItem }

constructor TAbstractItem.Create(const AParent: TAbstractItem);
var
  par: TFolderItem;
begin
  if (form = nil) then form := mainForm; // �������
  // initialization
  FIconFile := '';
  FParent := AParent;
  FLocation := point(0, 0);
  FText := 'New Item';
  // init icons
  FIcon := TBitmap.Create();
  FIcon.PixelFormat := pf32bit;
  FIcon.SetSize(48, 48);
  bm32Clear(FIcon);
  FNormIcon := TBitmap.Create();
  FNormIcon.PixelFormat := pf32bit;
  FBigIcon := TBitmap.Create;
  FBigIcon.PixelFormat := pf32bit;
  iconCreateSizes();
  // if parent class is Containet (folder), then we must add 'self' object to this container :)
  if (AParent <> nil) and (AParent.InheritsFrom(TFolderItem)) then
  begin
    par := TFolderItem(AParent);
    setLength(par.FItems, length(par.FItems) + 1);
    par.FItems[high(par.FItems)] := self;
  end;
end;

destructor TAbstractItem.Destroy;
var
  par: TFolderItem;
  i, j: integer;
begin
  FIcon.Free;
  FNormIcon.Free;
  FBigIcon.Free;
  // if parent is not null and parent is container, we must remove pointer to 'self' from parent
  if (FParent <> nil) and (FParent.InheritsFrom(TFolderItem)) then
  begin
    par := TFolderItem(FParent);
    // searching pointer to 'self'
    for i := 0 to high(par.FItems) do
      if (par.FItems[i] = self) then
      begin
        // removing self-pointer from parent
        for j := i to high(par.FItems) - 1 do
          par.FItems[i] := par.FItems[i + 1];
        setLength(par.FItems, high(par.FItems));
        // and breaking cycle
        break;
      end;
  end;
end;

function TAbstractItem.getFullSizeIcon: TBitmap;
begin
  result := FIcon;
end;

function TAbstractItem.getLeft: integer;
begin
  result := FLocation.X;
end;

function TAbstractItem.getLocation: TPoint;
begin
  result := FLocation;
end;

function TAbstractItem.getTop: integer;
begin
  result := FLocation.Y;
end;

procedure TAbstractItem.iconCreateSizes;
begin
  FNormIcon.SetSize(iconNormSize, iconNormSize);
  FBigIcon.SetSize(iconNormSize + iconExtendSize, iconNormSize + iconExtendSize);
  bm32Copy(FIcon, FNormIcon);
  bm32Copy(FIcon, FBigIcon);
end;

procedure TAbstractItem.setLocation(const x, y: integer);
begin
  FLocation.X := X;
  FLocation.Y := Y;
end;

procedure TAbstractItem.setIcon(const icon: TPngImage);
const
  MaxPixelCountA = 10240;
type
  PRGBAArray = ^TRGBAArray;
  TRGBAArray = array [0..MaxPixelCountA - 1] of TRGBQuad;
var
  j, i: Integer;
  fff: PRGBAArray;
  aaa: pByteArray;
  bmp: TBitmap;
begin
  bmp := TBitmap.Create;
  try
    icon.CreateAlpha;
    bmp.Assign(icon);
    bmp.PixelFormat := pf32bit;
    for i := 0 to bmp.Height - 1 do
    begin
      fff := bmp.ScanLine[i];
      aaa := icon.AlphaScanline[i];
      for j := 0 to bmp.Width - 1 do
        fff[j].rgbReserved := aaa[j];
    end;
    setIcon(bmp);
  finally
    bmp.Free;
  end;
end;

procedure TAbstractItem.setIcon(const icon: TBitmap);
begin
  FIcon.Assign(icon);
  iconCreateSizes();
end;

procedure TAbstractItem.setIcon(const fileName: string);
var
  ext: string;
  bm32: TBitmap;
begin
  if not FileExists(filename) then
    raise Exception.Create('Icon file not found!');
  ext := lowercase(ExtractFileExt(filename));
  if ext = '.png' then
  begin
    bm32 := BuildPNG2bm32(filename);
    setIcon(bm32);
    FIconFile := filename;
  end
  else raise Exception.Create('Unknown icon format!');
end;

procedure TAbstractItem.setLeft(const Value: integer);
begin
  FLocation.X := value;
end;

procedure TAbstractItem.setLocation(const location: TPoint);
begin
  FLocation := location;
end;

procedure TAbstractItem.setParent(const Value: TAbstractItem);
var
  par: TFolderItem;
  i, j: integer;
  inds: TSize;
  flag: boolean;
begin
  if (value <> nil) and not (value.InheritsFrom(TFolderItem)) then
    raise Exception.Create('ClassCastException: value is not TFolderItem');
  // �������� �� �������� ��������
  if (FParent <> nil) then
  begin
    par := TFolderItem(FParent);
    for i := 0 to high(par.FItems) do
      if (self = par.FItems[i]) then
      begin
        for j := i to high(par.FItems) - 1 do
          par.FItems[j] := par.FItems[j + 1];
        setLength(par.FItems, high(par.FItems));
        break;
      end;
  end;
  FParent := Value;
  // ���������� ���� � ������ ��������
  if (FParent <> nil) then
  begin
    par := TFolderItem(FParent);
    setLength(par.FItems, length(par.FItems) + 1);
    par.FItems[high(par.FItems)] := self;
    // ���� ��������� ������
    inds := form.getIconsIndexRect();
    flag := true;
    for j := 0 to inds.cy - 1 do
      if flag then
        for i := 0 to inds.cx - 1 do
          if par.findItemAt(point(i, j)) = -1 then
          begin
            setLocation(i, j);
            flag := false;
            break;
          end;
  end;
end;

procedure TAbstractItem.setTop(const Value: integer);
begin
  FLocation.Y := value;
end;

{ TFolderItem }

constructor TFolderItem.Create(const AParent: TAbstractItem);
begin
  inherited;
  text := 'New Folder';
  setLength(FItems, 0);
  setIcon(iconDefFolder);
end;

destructor TFolderItem.Destroy;
begin
  while length(FItems) > 0 do
    FItems[0].Free;
  inherited;
end;

function TFolderItem.findItemAt(const p: TPoint): integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to high(Fitems) do
    if (FItems[i].left = p.X) and (FItems[i].top = p.Y) then
    begin
      result := i;
      break;
    end;
end;

function TFolderItem.getCount: integer;
begin
  result := length(FItems);
end;

function TFolderItem.getItem(index: integer): TAbstractItem;
begin
  result := FItems[index];
end;

{ TLinkItem }

constructor TLinkItem.Create(const AParent: TAbstractItem);
begin
  inherited;
  setIcon(iconDefLink);
end;

destructor TLinkItem.Destroy;
begin

  inherited;
end;



initialization
  iconDir := ExtractFilePath(ParamStr(0)) + '\Icons\';
  iconNormSize := 64;
  iconExtendSize := 16;
  iconDefFolder := buildPNG2bm32(iconDir + 'folder.png');
  iconDefLink := buildPNG2bm32(iconDir + 'unknown link.png');
  form := nil;

end.
