unit SettingsUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TSettingsForm = class(TForm)
    PageControl1: TPageControl;
    btnOk: TButton;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    cbAutoStart: TCheckBox;
    cbStayOnTop: TCheckBox;
    txtPanelSkinName: TEdit;
    Button1: TButton;
    tbPanelTransparent: TTrackBar;
    Label1: TLabel;
    procedure btnOkClick(Sender: TObject);
    procedure cbStayOnTopClick(Sender: TObject);
    procedure tbPanelTransparentChange(Sender: TObject);
  private

  public

  end;

var
  SettingsForm: TSettingsForm;

implementation

uses MainUnit;

{$R *.dfm}

procedure TSettingsForm.btnOkClick(Sender: TObject);
begin
  hide;
end;

procedure TSettingsForm.cbStayOnTopClick(Sender: TObject);
begin
  if cbAutoStart.Checked then
    MainForm.FormStyle := fsStayOnTop
  else
    MainForm.FormStyle := fsNormal;
end;

procedure TSettingsForm.tbPanelTransparentChange(Sender: TObject);
begin
  Label1.Caption := 'Transparent: ' + intToStr(tbPanelTransparent.Position * 100 div 255) + '%';
  MainForm.paneTransparent := 255 - tbPanelTransparent.Position;
  MainForm.renderScene();
end;

end.
