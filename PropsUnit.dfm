object PropsForm: TPropsForm
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'Properties'
  ClientHeight = 330
  ClientWidth = 345
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 10
    Width = 60
    Height = 13
    Caption = 'Item caption'
  end
  object imgIcon: TImage
    Left = 8
    Top = 56
    Width = 81
    Height = 81
  end
  object pcProps: TPageControl
    Left = 8
    Top = 152
    Width = 329
    Height = 137
    ActivePage = tsLink
    TabOrder = 0
    object tsLink: TTabSheet
      Caption = 'Link'
    end
    object tsFolder: TTabSheet
      Caption = 'Folder'
      ImageIndex = 1
    end
  end
  object btnOk: TButton
    Left = 182
    Top = 295
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = btnOkClick
  end
  object txtText: TEdit
    Left = 8
    Top = 29
    Width = 329
    Height = 21
    TabOrder = 2
    Text = 'Item Name'
  end
  object btnCancel: TButton
    Left = 262
    Top = 295
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object Button1: TButton
    Left = 232
    Top = 112
    Width = 101
    Height = 25
    Caption = 'Browse Icon ...'
    TabOrder = 4
    OnClick = Button1Click
  end
  object dlgOpen: TFileOpenDialog
    FavoriteLinks = <>
    FileTypes = <>
    Options = [fdoForceFileSystem]
    Left = 168
    Top = 88
  end
end
